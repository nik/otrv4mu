SRCPATH = /usr/local/src/cmurphi/src/
INCLUDEPATH = /usr/local/src/cmurphi/include

CXX = g++

CFLAGS = # -w add this to disable warnings, due to include problems

OFLAGS = -O2

#Murphi options
MURPHIOPTS = -b -c

all: otrv4 otrv4.cache otrv4.disk
splitfile: otrv4.cache.splitfile otrv4.disk.splitfile

# rules for compiling
otrv4: otrv4.cpp
	${CXX} ${CFLAGS} ${OFLAGS} -o otrv4 otrv4.cpp -I${INCLUDEPATH} -lm

otrv4.cache: otrv4.cache.cpp
	${CXX} ${CFLAGS} ${OFLAGS} -o otrv4.cache otrv4.cache.cpp -I${INCLUDEPATH} -lm

otrv4.cache.splitfile: otrv4.cache.cpp
	${CXX} ${CFLAGS} ${OFLAGS} -o otrv4.cache.splitfile otrv4.cache.cpp -I${INCLUDEPATH} -lm -DSPLITFILE

otrv4.disk.splitfile: otrv4.disk.cpp
	${CXX} ${CFLAGS} ${OFLAGS} -o otrv4.disk.splitfile otrv4.disk.cpp -I${INCLUDEPATH} -lm -DSPLITFILE

otrv4.disk: otrv4.disk.cpp
	${CXX} ${CFLAGS} ${OFLAGS} -o otrv4.disk otrv4.disk.cpp -I${INCLUDEPATH} -lm

otrv4.cpp: otrv4.m
	${SRCPATH}mu otrv4.m

otrv4.cache.cpp: otrv4.m
	${SRCPATH}mu --cache -b -c otrv4.m
	mv otrv4.cpp otrv4.cache.cpp

otrv4.disk.cpp: otrv4.m
	${SRCPATH}mu --disk otrv4.m
	mv otrv4.cpp otrv4.disk.cpp

clean:
	rm -f *.cpp otrv4 otrv4.cache otrv4.disk otrv4.cache.splitfile otrv4.disk.splitfile

runake:
	make otrv4
	./otrv4 -tv -ndl
