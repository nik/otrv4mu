## Notes

### States:

* Protocol state machine:
  - START
  - WAITING_AUTH_R
  - WAITING_AUTH_I
  - ENCRYPTED_MESSAGES
  - FINISHED

* SMP state machine:
  - SMPSTATE_EXPECT1
  - SMPSTATE_EXPECT2
  - SMPSTATE_EXPECT3
  - SMPSTATE_EXPECT4

### Take into account:

- Whitespace tags are not be sent in ENCRYPTED_MESSAGES state.
- Transition between interactive and non-interactive.
- Receiving v3 messages at any point.
- Receiving a non-interactive-auth message at any point.
- Receiving a identity message at any point.
- Receiving a auth-r message at any point.
- Receiving a auth-i message at any point.

### Add to the protocol

- Whitespace tags are not be sent in ENCRYPTED_MESSAGES state.
- Are we going to allow receiving query messages in any state?
- Are we going to allow receiving identity messages in encrypted state?
- Are we going to allow receiving non-interactive-auth messages in encrypted state?
